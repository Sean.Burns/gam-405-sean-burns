﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;

using Path = System.Collections.Generic.List<ClipperLib.IntPoint>;
using Paths = System.Collections.Generic.List<System.Collections.Generic.List<ClipperLib.IntPoint>>;


public class NavMesh : MonoBehaviour
{


    // polygon class within NavMesh------------------------------------------
   public class Polygon
   {
        public List<Vector3> points = new List<Vector3>();
        public Path path = new Path();
   }
   //------------------------------------------------------------------------

    // Triangle class used for triangulation---------------------------------
    public class Triangle
    {
        // vertices
        public Vector3 v1;
        public Vector3 v2;
        public Vector3 v3;

        // centroid
        public Vector3 centroid;

        // middle points of each edge
        public List<Vector3> edgePoints = new List<Vector3>();

        // distance from centroid to edge points
        public float innerDistance;

        // constructor to add vertices
        public Triangle(Vector3 add1, Vector3 add2, Vector3 add3)
        {
            v1 = add1;
            v2 = add2;
            v3 = add3;
        }
    }
    //-----------------------------------------------------------------------


    // VARIABLES-------------------------------------------------------------
    // array for gameobjects to be made into polygons
    public GameObject[] aiFloorObjects;

    // List for polygons in nav mesh
    public List<Polygon> meshPolys = new List<Polygon>();

    // nav mesh paths
    public Paths mesh = new Paths();

    // points of nav mesh used for drawing and trinagulation
     public List<Vector3> navMeshPoints = new List<Vector3>();

    // list of triangles after triangulation
    public List<Triangle> navMeshTriangulated = new List<Triangle>();
    //------------------------------------------------------------------------


    // START FUNCTION---------------------------------------------------------
    private void Awake()
    {
        CreatePolygons();
        PolyCombine();
        navMeshPoints = NavPoints(mesh);
        Triangulate();
    }
    //------------------------------------------------------------------------


    // POLYGON GENERATION-----------------------------------------------------
    // fill the nav mesh with polygons
    public void CreatePolygons()
    {
        // find all walkable ai surfaces
        aiFloorObjects = GameObject.FindGameObjectsWithTag("AI Surface");

        // loop through and add polygons into nav mesh
        for (int i = 0; i < aiFloorObjects.Length; i++)
        {
            Polygon poly = new Polygon();

            // add points 
            poly.points.Add(aiFloorObjects[i].transform.TransformPoint(new Vector3(-0.5f, 0.5f, -0.5f)));
            poly.points.Add(aiFloorObjects[i].transform.TransformPoint(new Vector3(-0.5f, 0.5f, 0.5f)));
            poly.points.Add(aiFloorObjects[i].transform.TransformPoint(new Vector3(0.5f, 0.5f, 0.5f)));
            poly.points.Add(aiFloorObjects[i].transform.TransformPoint(new Vector3(0.5f, 0.5f, -0.5f)));

            // make path points from poly points 
            IntPoint p1 = new IntPoint((long)poly.points[0].x, (long)poly.points[0].z);
            IntPoint p2 = new IntPoint((long)poly.points[1].x, (long)poly.points[1].z);
            IntPoint p3 = new IntPoint((long)poly.points[2].x, (long)poly.points[2].z);
            IntPoint p4 = new IntPoint((long)poly.points[3].x, (long)poly.points[3].z);

            // add points into polygon path 
            poly.path.Add(p1);
            poly.path.Add(p2);
            poly.path.Add(p3);
            poly.path.Add(p4);

            // add polygon into meshPolys
            meshPolys.Add(poly);
        }
    }
    //------------------------------------------------------------------------


    // POLYGON CLEANUP--------------------------------------------------------
    // combine polygons
    public void PolyCombine()
    {
        // loop through each polygon and add path to paths
        foreach (Polygon poly in meshPolys)
        {
            mesh.Add(poly.path);
        }

        // union polys into one poly
        Clipper clip = new Clipper();
        clip.AddPaths(mesh, PolyType.ptClip, true);
        clip.Execute(ClipType.ctUnion, mesh, PolyFillType.pftEvenOdd, PolyFillType.pftNonZero);

        // shrink poly to be within walls
        ClipperOffset contract = new ClipperOffset();
        contract.AddPaths(mesh, JoinType.jtMiter, EndType.etClosedPolygon);
        contract.Execute(ref mesh, -1);

    }
    //------------------------------------------------------------------------


    // DRAW FUNCTION----------------------------------------------------------
    // Draw NavMesh and Polygons
    private void OnDrawGizmos()
    {
        // draw polygons
        // loop through each polygon
        /*foreach (Polygon polygon in meshPolys)
        {
            // loop through each point in polygon
            for (int i = 0; i < polygon.points.Count; i++)
            {
                // draw line

                if (i == polygon.points.Count - 1)
                {
                    Gizmos.DrawLine(polygon.points[i], polygon.points[0]);
                }
                else
                {
                    Gizmos.DrawLine(polygon.points[i], polygon.points[i + 1]);
                }
            }
        }*/

        // draw nav mesh
        Gizmos.color = new Color(0, 1, 0, 1);

        // draw lines between points
        for (int i = 0; i < navMeshPoints.Count; i++)
        {
            //Debug.Log("Position: " + navMeshPoints[i]);
            if (i == navMeshPoints.Count - 1)
            {
                Gizmos.DrawLine(navMeshPoints[i], navMeshPoints[0]);
            }
            else
            {
                Gizmos.DrawLine(navMeshPoints[i], navMeshPoints[i + 1]);
            }
        }

        // draw triangulated nav mesh
        Gizmos.color = new Color(1, 0, 0, 1);
        Vector3 size = new Vector3(0.2f, 0.2f, 0.2f);

        // loop through triangulated navmesh drawing each triangle
        foreach(Triangle triangle in navMeshTriangulated)
        {
            Gizmos.DrawLine(triangle.v1, triangle.v2);
            Gizmos.DrawLine(triangle.v2, triangle.v3);
            Gizmos.DrawLine(triangle.v3, triangle.v1);

            for( int i = 0; i < triangle.edgePoints.Count; i++)
            {
                Gizmos.DrawCube(triangle.edgePoints[i], size);
            }
        }
    }
    //------------------------------------------------------------------------


    // FILL LIST WITH POINTS OF NAV MESH -------------------------------------
    public List<Vector3> NavPoints(Paths paths)
    {
        // create points to draw from mesh
        List<Vector3> navPoints = new List<Vector3>();

        for (int i = 0; i < paths.Count; i++)
        {
            for (int j = 0; j < paths[i].Count; j++)
            {
                navPoints.Add(new Vector3(paths[i][j].X, aiFloorObjects[j].transform.position.y + 0.5f, paths[i][j].Y));
            }
        }

        return navPoints;
    }
    //------------------------------------------------------------------------


    // triangulate paths -----------------------------------------------------
    private void Triangulate()
    {
        // container to store points already used in creating a triangle
        List<Vector3> triangulated = new List<Vector3>();

        // int used for checking if overlapping triangles
        int shareVertices = 0;

        // bool to check overlap
        bool overlap = false; 

        // TODO : truangulate mesh paths

        // loop through and find angles
        for (int i = 0; i < navMeshPoints.Count; i++)
        {

            shareVertices = 0;
            overlap = false;

            if (i == 0) // first vertex
            {
                // find vertices for triangle
                Vector2 a = new Vector2(navMeshPoints[navMeshPoints.Count - 1].x, navMeshPoints[navMeshPoints.Count - 1].z);
                Vector2 b = new Vector2(navMeshPoints[i].x, navMeshPoints[i].z);
                Vector2 c = new Vector2(navMeshPoints[i + 1].x, navMeshPoints[i + 1].z);

                float angle = FindAngle(a, b, c);
                //Debug.Log(" angle : " + angle);

                foreach(Triangle triangle in navMeshTriangulated)
                {
                    shareVertices = 0;

                    // check if triangles share any vertices (with point opposite of hypotenuse)
                    //if (navMeshPoints[navMeshPoints.Count - 1] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[navMeshPoints.Count - 1] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[navMeshPoints.Count - 1] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[i] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[i + 1] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i + 1] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i + 1] == triangle.v3) { shareVertices += 1; }

                    if (shareVertices >= 1) { overlap = true; }
                }

                // if no overlap create triangle
                if (!overlap)
                {
                    Triangle tri = new Triangle(navMeshPoints[navMeshPoints.Count - 1], navMeshPoints[i], navMeshPoints[i + 1]);
                    TriCalculate(tri);

                    // if angle is eligible add triangle to mesh
                    if (angle <= 160 && angle > 20)
                    {
                        navMeshTriangulated.Add(tri);
                    }
                }

            }
            else if (i == navMeshPoints.Count - 1) // last vertex
            {
                // find vertices for triangle
                Vector2 a = new Vector2(navMeshPoints[i - 1].x, navMeshPoints[i - 1].z);
                Vector2 b = new Vector2(navMeshPoints[i].x, navMeshPoints[i].z);
                Vector2 c = new Vector2(navMeshPoints[0].x, navMeshPoints[0].z);

                float angle = FindAngle(a, b, c);
               //Debug.Log(" angle : " + angle);

                foreach (Triangle triangle in navMeshTriangulated)
                {
                    shareVertices = 0;

                    // check if triangles share any vertices (with point opposite of hypotenuse)
                    //if (navMeshPoints[i - 1] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i - 1] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i - 1] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[i] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[0] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[0] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[0] == triangle.v3) { shareVertices += 1; }

                    if (shareVertices >= 1) { overlap = true; }
                }

                // if no overlap create triangle
                if (!overlap)
                {
                    Triangle tri = new Triangle(navMeshPoints[i - 1], navMeshPoints[i], navMeshPoints[0]);
                    TriCalculate(tri);

                    // if angle is eligible add triangle to mesh
                    if (angle <= 160 && angle > 20)
                    {
                        navMeshTriangulated.Add(tri);
                    }
                }

            }
            else // any other vertex
            {
                // find vertices for triangle
                Vector2 a = new Vector2(navMeshPoints[i - 1].x, navMeshPoints[i - 1].z);
                Vector2 b = new Vector2(navMeshPoints[i].x, navMeshPoints[i].z);
                Vector2 c = new Vector2(navMeshPoints[i + 1].x, navMeshPoints[i + 1].z);

                float angle = FindAngle(a, b, c);
                //Debug.Log(" angle : " + angle);

                foreach (Triangle triangle in navMeshTriangulated)
                {
                    shareVertices = 0;

                    // check if triangles share any vertices (with point opposite of hypotenuse)
                    //if (navMeshPoints[i - 1] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i - 1] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i - 1] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[i] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i] == triangle.v3) { shareVertices += 1; }

                    //if (navMeshPoints[i + 1] == triangle.v1) { shareVertices += 1; }
                    if (navMeshPoints[i + 1] == triangle.v2) { shareVertices += 1; }
                    //if (navMeshPoints[i + 1] == triangle.v3) { shareVertices += 1; }

                    if (shareVertices >= 1) { overlap = true; }
                }

                // if no overlap create triangle
                if (!overlap)
                {
                    Triangle tri = new Triangle(navMeshPoints[i - 1], navMeshPoints[i], navMeshPoints[i + 1]);
                    TriCalculate(tri);

                    // if angle is eligible add triangle to mesh
                    if (angle <= 160 && angle > 20)
                    {
                        navMeshTriangulated.Add(tri);
                    }
                }

            }
            
        }
    }
    //-------------------------------------------------------------------------


    // find angle--------------------------------------------------------------
    private float FindAngle(Vector2 a, Vector2 b, Vector2 c)
    {
        // check if angle goes outside mesh
        Vector2 midPoint;
        midPoint = (a + c) * 0.5f;

        // midpoint to check if triangle's hypotenus is over no gameobjects
        Vector3 midPointCheck;
        midPointCheck = new Vector3(midPoint.x, 0.5f, midPoint.y);


        Collider[] intersecting = Physics.OverlapSphere(midPointCheck, 0.01f);
        if (intersecting.Length == 0)
        {
            // return negative if nothing is intersecting as the length is 0
            return - Vector2.Angle(a - b, c - b);
        }
        else
        {
            // return positive if over mesh run if something is intersecting it
            return Vector2.Angle(a - b, c - b);
        }

    }
    //--------------------------------------------------------------------------


    // calculate centroid and edge points---------------------------------------
    private void TriCalculate(Triangle tri)
    {
        // centroid calculation
        float x = ((tri.v1.x + tri.v2.x + tri.v3.x) / 3);
        float y = ((tri.v1.y + tri.v2.y + tri.v3.y) / 3);
        float z = ((tri.v1.z + tri.v2.z + tri.v3.z) / 3);

        tri.centroid = new Vector3(x, y, z);

        // edge points calculation

        // edgepoint 1 (v1->v2)
        Vector3 e1 = (tri.v1 + tri.v2) * 0.5f;
        // edgepoint 1 (v2->v3)
        Vector3 e2 = (tri.v2 + tri.v3) * 0.5f;
        // edgepoint 1 (v3->v1)
        Vector3 e3 = (tri.v3 + tri.v1) * 0.5f;

        // add edgepoints to list
        tri.edgePoints.Add(e1);
        tri.edgePoints.Add(e2);
        tri.edgePoints.Add(e3);

        // distance from centroid to edgepoints calculation
        float e1Dist = Vector3.Distance(tri.centroid, e1);
        float e2Dist = Vector3.Distance(tri.centroid, e2);
        float e3Dist = Vector3.Distance(tri.centroid, e3);

        tri.innerDistance = ((e1Dist + e2Dist + e3Dist) / 3);
    }
    //--------------------------------------------------------------------------
}
