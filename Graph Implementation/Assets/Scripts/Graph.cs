﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    // container for nodes
    public List<PathNode> graphNodes;

    // enum to check if graph is a nav mesh
    public enum GraphType { BasicGraph, NavMeshGraph };
    public GraphType graphType;

    // nav mesh obj
    public NavMesh navMesh;
    // node prefab object
    public GameObject nodePrefab;

    // Add new node to graph
    public void AddNode(PathNode aPathNode)
    {
        // loop through and connect if close enough
        if (graphNodes.Count > 0)
        {

           GameObject node = aPathNode.gameObject;

            for (int i = 0; i < graphNodes.Count; i++)
            {
                GameObject tempNode = graphNodes[i].gameObject;

                // distance check
                float distance = Vector3.Distance(node.transform.position, tempNode.transform.position);

                // connection if within certain range
                if (graphType == GraphType.BasicGraph)
                {
                    if (distance <= 2.1)
                    {
                        aPathNode.connectedNodes.Add(graphNodes[i]);
                        graphNodes[i].connectedNodes.Add(aPathNode);
                    }
                }
                else if(graphType == GraphType.NavMeshGraph)
                {
                    // do nothing
                    // connection will happen later
                }
            }
        }

        // add node to graph data structure
        graphNodes.Add(aPathNode);

    }

    // Draw nodes and connections
    private void OnDrawGizmos()
    {
        for (int i = 0; i < graphNodes.Count; i++)
        {
            // draw node
            Gizmos.DrawSphere(graphNodes[i].transform.position, 0.5f);
            
            // draw all connections for node
            /*for (int j = 0; j < graphNodes[i].connectedNodes.Count; j++)
            {
                Gizmos.DrawLine(graphNodes[i].transform.position, graphNodes[i].connectedNodes[j].transform.position);
            }*/
        }
        
    }

    // PATHFINDING ALGORITHMS

    public List<PathNode> BreadthFirstSearch(PathNode start, PathNode end)
    {
        // frontier queue
        Queue<PathNode> frontier = new Queue<PathNode>();
        frontier.Enqueue(start);

        // store path information (node, camefrom)
        Dictionary<PathNode, PathNode> cameFrom = new Dictionary<PathNode, PathNode>();
        cameFrom.Add(start, null);

        // traversal node
        //PathNode current = new PathNode();

        while (frontier.Count != 0)
        {
            // traversal node
            PathNode current = frontier.Dequeue();

            for (int i = 0; i < current.connectedNodes.Count; i++)
            {
                if (!cameFrom.ContainsKey(current.connectedNodes[i]))
                {
                    frontier.Enqueue(current.connectedNodes[i]);

                    cameFrom.Add(current.connectedNodes[i], current);
                }
            }
        }

        // create path

        bool pathFound = false;
        PathNode index = end;
        List<PathNode> returnPath = new List<PathNode>();

        // set first pathnode to the end point
        returnPath.Add(index);

        // loop to find path
        while (!pathFound)
        {
            // set index to node that added it and add into path
            index = cameFrom[index];
            returnPath.Add(index);

            // end loop condition
            if (cameFrom[index] == null)
            {
                pathFound = true;
            }
        }

        // reverse path
        returnPath.Reverse();

        return returnPath;
    }

    public List<PathNode> HeuristicSearch(PathNode start, PathNode end)
    {
        // frontier data structure
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>();
        frontier.Enqueue(start, 1);

        // store path information (node, camefrom)
        Dictionary<PathNode, PathNode> cameFrom = new Dictionary<PathNode, PathNode>();
        cameFrom.Add(start, null);

        // return path data structure 
        List<PathNode> returnPath = new List<PathNode>();

        // other needed variables
        bool pathFound = false;
        PathNode index = end;

        while (frontier.Count() != 0)
        {
            // traversal node
            PathNode current = frontier.Dequeue();

            for (int i = 0; i < current.connectedNodes.Count; i++)
            {
                if (!cameFrom.ContainsKey(current.connectedNodes[i]))
                {
                    // calculate distance and add into queue with priority
                    float distance = Heuristic(current.connectedNodes[i], end);
                    frontier.Enqueue(current.connectedNodes[i], (int)distance);

                    // add to cameFrom Dictionary
                    cameFrom.Add(current.connectedNodes[i], current);
                }
            }
        }

        // Create Path

        // set first pathnode to the end point
        returnPath.Add(index);

        // loop to find path
        while (!pathFound)
        {
            // set index to node that added it and add into path
            index = cameFrom[index];
            returnPath.Add(index);

            // end loop condition
            if (cameFrom[index] == null)
            {
                pathFound = true;
            }
        }

        // reverse path
        returnPath.Reverse();

        return returnPath;
    }

    public List<PathNode> DijkstrasSearch(PathNode start, PathNode end)
    {
        // frontier data structure
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>();
        frontier.Enqueue(start, 1);

        // store path information (node, camefrom)
        Dictionary<PathNode, PathNode> cameFrom = new Dictionary<PathNode, PathNode>();
        cameFrom.Add(start, null);

        // return path data structure 
        List<PathNode> returnPath = new List<PathNode>();

        // other needed variables
        bool pathFound = false;
        float totalWeight = 0;
        PathNode index = end;

        while (frontier.Count() != 0)
        {
            // traversal node and weight calculation
            PathNode current = frontier.Dequeue();
            totalWeight += current.weight;

            for (int i = 0; i < current.connectedNodes.Count; i++)
            {
                if (!cameFrom.ContainsKey(current.connectedNodes[i]))
                {
                    // calculate lowest weight and add to queue
                    frontier.Enqueue(current.connectedNodes[i], (int)(current.connectedNodes[i].weight + totalWeight));

                    // add to cameFrom Dictionary
                    cameFrom.Add(current.connectedNodes[i], current);
                }
            }
        }

        // Create Path

        // set first pathnode to the end point
        returnPath.Add(index);

        // loop to find path
        while (!pathFound)
        {
            // set index to node that added it and add into path
            index = cameFrom[index];
            returnPath.Add(index);

            // end loop condition
            if (cameFrom[index] == null)
            {
                pathFound = true;
            }
        }

        // reverse path
        returnPath.Reverse();

        return returnPath;
    }

    public List<PathNode> AStarSearch(PathNode start, PathNode end)
    {
        // frontier data structure
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>();
        frontier.Enqueue(start, 1);

        // store path information (node, camefrom)
        Dictionary<PathNode, PathNode> cameFrom = new Dictionary<PathNode, PathNode>();
        cameFrom.Add(start, null);

        // return path data structure 
        List<PathNode> returnPath = new List<PathNode>();

        // other needed variables
        bool pathFound = false;
        float totalWeight = 0;
        PathNode index = end;

        while (frontier.Count() != 0)
        {
            // traversal node and weight calculation
            PathNode current = frontier.Dequeue();
            totalWeight += current.weight;

            for (int i = 0; i < current.connectedNodes.Count; i++)
            {
                if (!cameFrom.ContainsKey(current.connectedNodes[i]))
                {
                    // calculate distance from heuristic function
                    float distance = Heuristic(current.connectedNodes[i], end);

                    // calculate weight
                    float weight = current.connectedNodes[i].weight + totalWeight;

                    // calculate priority
                    int priority = (int)distance + (int)weight;

                    // enqueue node with priority
                    frontier.Enqueue(current.connectedNodes[i], priority);

                    // add to cameFrom Dictionary
                    cameFrom.Add(current.connectedNodes[i], current);
                }
            }
        }

        // Create Path

        // set first pathnode to the end point
        returnPath.Add(index);

        // loop to find path
        while (!pathFound)
        {
            // set index to node that added it and add into path
            index = cameFrom[index];
            returnPath.Add(index);

            // end loop condition
            if (cameFrom[index] == null)
            {
                pathFound = true;
            }
        }

        // reverse path
        returnPath.Reverse();

        return returnPath;
    }

    // Heuristic function
    public float Heuristic(PathNode a, PathNode b)
    {
        return Vector3.Distance(a.gameObject.transform.position, b.gameObject.transform.position);
    }

    // find closest node to object
    public PathNode FindClosestNode(Vector3 aPosition)
    {
        //return node
        PathNode returnNode = new PathNode();

        // float used to keep lowest distance
        float lowestDist;

        // loop through graph and collect distances
        for (int i = 0; i < graphNodes.Count; i++)
        {
            float distance = Vector3.Distance(graphNodes[i].gameObject.transform.position, aPosition);
            graphNodes[i].mouseDistance = distance;
        }

        lowestDist = graphNodes[0].mouseDistance;

        // loop through and get lowest distance 
        for (int i = 0; i < graphNodes.Count; i++)
        {
            if (lowestDist > graphNodes[i].mouseDistance)
            {
                lowestDist = graphNodes[i].mouseDistance;
            }
        }

        // get return node
        for (int i = 0; i < graphNodes.Count; i++)
        {
            float distance = Vector3.Distance(graphNodes[i].gameObject.transform.position, aPosition);

            if (distance <= lowestDist)
            {
                returnNode = graphNodes[i];
                break;
            }
        }

        return returnNode;
    }


    // find closest end position to mouse position 
    public Vector3 FindEndPosition(Vector3 mousePos)
    {
        // end position
        Vector3 endPos = new Vector3();

        // structure to keep distances and where they were found
        Dictionary<GameObject, float> distancesFromMouse = new Dictionary<GameObject, float>();

        // float to track lowest distance
        float lowestDist = 0;

        // loop throught floor objects and find all distances 
        for (int i = 0; i < navMesh.aiFloorObjects.Length; i++)
        {
            // calculate distance
            float distance = Vector3.Distance(navMesh.aiFloorObjects[i].transform.position, mousePos);

            // add to containers
            distancesFromMouse.Add(navMesh.aiFloorObjects[i], distance);
        }

        // loop through and find lowest distance 
       for (int i = 0; i < distancesFromMouse.Count; i++)
       {
            if ( i == 0)
            {
                // get first distance
                if (distancesFromMouse[navMesh.aiFloorObjects[i]] <= distancesFromMouse[navMesh.aiFloorObjects[i + 1]])
                {
                    lowestDist = distancesFromMouse[navMesh.aiFloorObjects[i]];
                }
                else
                {
                    lowestDist = distancesFromMouse[navMesh.aiFloorObjects[i + 1]];
                }
            }
            else
            {
                // find lowest distance
                if (distancesFromMouse[navMesh.aiFloorObjects[i]] <= lowestDist)
                {
                    lowestDist = distancesFromMouse[navMesh.aiFloorObjects[i]];
                }
            }
       }

        // loop through to find return vector
        for (int i = 0; i < distancesFromMouse.Count; i++)
        {
            if (distancesFromMouse[navMesh.aiFloorObjects[i]] == lowestDist)
            {
                endPos = navMesh.aiFloorObjects[i].transform.position;
            }
        }

        return new Vector3(endPos.x, 1, endPos.z);
    }


    // Smoothing of ai moving path
    // smooths by choosing a random point between nodes and adding a node at the random point
    // call smooth function multiple times to add more random nodes
    public List<PathNode> SmoothPath(List<PathNode> aPath)
    {
        // path to be returned
        List<PathNode> returnPath = new List<PathNode>();

        // loop through path adding smoothing nodes
        for (int i = 1; i < aPath.Count; i++)
        {
            // generate random point floats
            float randX = Random.Range(aPath[i - 1].transform.position.x - 1, aPath[i].transform.position.x + 1);
            float randZ = Random.Range(aPath[i - 1].transform.position.z - 1, aPath[i].transform.position.z + 1);

            // generate random point
            Vector3 newPoint = new Vector3(randX, 0.5f, randZ);

            // create new pathnode
            GameObject nodeSmoothClone = Instantiate(nodePrefab);

            // set nodes position
            nodeSmoothClone.transform.position = newPoint;

            // connect nodes
            nodeSmoothClone.GetComponent<PathNode>().connectedNodes.Add(aPath[i]);
            nodeSmoothClone.GetComponent<PathNode>().connectedNodes.Add(aPath[i - 1]);

            aPath[i].connectedNodes.Add(nodeSmoothClone.GetComponent<PathNode>());
            aPath[i - 1].connectedNodes.Add(nodeSmoothClone.GetComponent<PathNode>());

            // add nodes into new path
            returnPath.Add(aPath[i - 1]);
            returnPath.Add(nodeSmoothClone.GetComponent<PathNode>());
            returnPath.Add(aPath[i]);
        }

        // loop through and delete any repeat nodes in path
        for (int i = 1; i < returnPath.Count; i++)
        {
            // check if nodes have the same position
            if (returnPath[i - 1].transform.position == returnPath[i].transform.position)
            {
                // remove repeated node
                returnPath.Remove(returnPath[i - 1]);
            }
        }

        return returnPath;
    }


    // connection of triangulated navmesh graph
    public void ConnectNodes()
    {
        // connection based on distance 
        for (int i = 0; i < graphNodes.Count; i++)
        {
            GameObject node = graphNodes[i].gameObject;

            for (int j = 0; j < graphNodes.Count; j++)
            {
                GameObject tempNode = graphNodes[j].gameObject;

                // distance check
                float distance = Vector3.Distance(node.transform.position, tempNode.transform.position);

                // connection if within certain range

                //Debug.Log("Distance Check : " + graphNodes[i].distanceCheck);
                if (distance <= graphNodes[i].distanceCheck + 2.1) // add 2.1 to make up for avg of distance check float
                {
                    graphNodes[i].connectedNodes.Add(graphNodes[j]);
                    graphNodes[j].connectedNodes.Add(graphNodes[i]);
                }
            }

        }

    }

    // awake function
    void Awake()
    {
        // generate graph nodes from triangulated mesh
        if (graphType == GraphType.NavMeshGraph)
        {
            for (int i = 0; i < navMesh.navMeshTriangulated.Count; i++)
            {
                // clone prefab
                GameObject nodeCloneCenter = Instantiate(nodePrefab);
                // set position
                nodeCloneCenter.transform.position = navMesh.navMeshTriangulated[i].centroid;

                // edgepoint nodes
                foreach(Vector3 edgePoint in navMesh.navMeshTriangulated[i].edgePoints)
                {
                    // clone prefab
                    GameObject nodeCloneEdge = Instantiate(nodePrefab);
                    // set position
                    nodeCloneEdge.transform.position = edgePoint;
                }
            }

            int index = 0;
            int triangleID = 1;
            
            for (int i = 0; i < navMesh.navMeshTriangulated.Count; i++)
            {
                // set distance for distance check
                graphNodes[index].distanceCheck = navMesh.navMeshTriangulated[i].innerDistance;
                graphNodes[index + 1].distanceCheck = navMesh.navMeshTriangulated[i].innerDistance;
                graphNodes[index + 2].distanceCheck = navMesh.navMeshTriangulated[i].innerDistance;
                graphNodes[index + 3].distanceCheck = navMesh.navMeshTriangulated[i].innerDistance;

                // set triangle index 
                graphNodes[index].triangleIdx = triangleID;
                graphNodes[index + 1].triangleIdx = triangleID;
                graphNodes[index + 2].triangleIdx = triangleID;
                graphNodes[index + 3].triangleIdx = triangleID;

                triangleID += 1;
                index += 4;
            }

            // connect the nodes 
            ConnectNodes();

        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}


