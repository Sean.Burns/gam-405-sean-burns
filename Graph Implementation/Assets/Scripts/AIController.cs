﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    // graph 
    public Graph graph;

    // path finding enum
    public enum PathFinding { BreadthFirstSearch, HeuristicSearch, DijkstrasSearch, AStarSearch };
    public PathFinding algorithm;

    // smoothing bool
    public bool smoothing = false;

    // smoothing count (how many times the path should be smoothed)
    [Range(1, 3)]
    public int smoothingCount;

    // path
    public List<PathNode> path = new List<PathNode>();

    // other variables
    private Vector3 lastMousePos;
    public float speed = 2;
    private PathNode start;
    private Vector3 startPos;
    private PathNode end;
    private Vector3 endPos; // end point to move to after node
    private int nextPosIndex;
    private Vector3 nextPos;

    // color variables 
    public float r = 0.0f;
    public float g = 0.0f;
    public float b = 0.0f;
    public float a = 1.0f;
    public Vector4 color;


    // set color variables when awake
    private void Start()
    {
        color = new Vector4(r, g, b, a);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // GET MOUSE POSITION
            Plane plane = new Plane(Vector3.up, 0);
            Vector3 mousePos = new Vector3();
            float pos;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // get start position
            startPos = this.transform.position;

            if (plane.Raycast(ray, out pos))
            {
                mousePos = ray.GetPoint(pos);
                endPos = graph.FindEndPosition(mousePos);
            }

            // get start and end nodes
            start = graph.FindClosestNode(gameObject.transform.position);
            end = graph.FindClosestNode(mousePos);

            // get path
            if (algorithm == PathFinding.BreadthFirstSearch)
            {
                path = graph.BreadthFirstSearch(start, end);
            }
            else if (algorithm == PathFinding.HeuristicSearch)
            {
                path = graph.HeuristicSearch(start, end);
            }
            else if (algorithm == PathFinding.DijkstrasSearch)
            {
                path = graph.DijkstrasSearch(start, end);
            }
            else if (algorithm == PathFinding.AStarSearch)
            {
                path = graph.AStarSearch(start, end);
            }

            // smooth path for nav mesh
             if (smoothing)
             {
                // smooth as many times as specified
                for (int i = 0; i < smoothingCount; i++)
                {
                    path = graph.SmoothPath(path);
                }
             }

            // set new vector to prevent from ai going into floor
            nextPos = new Vector3(path[0].gameObject.transform.position.x, 1f, path[0].gameObject.transform.position.z); 

        }

        // update mouse position
        lastMousePos = Input.mousePosition;

        // if there is a path move AI
        if (start != null)
        {
            //moving = true;
            MoveAI(path);
        }

        //moving = false;
    }

    public void MoveAI (List<PathNode> path)
    {
        // check if AI reached next node
        if (transform.position == nextPos) 
        {
            // increase node index to get the next node in the path
            nextPosIndex++;

            // if AI is at end of path end movement
            if (nextPosIndex > path.Count)
            {
                return;
            }

            // set next position
            if (nextPosIndex == path.Count)
            {
                // set position to end position if ai is at the last node
                nextPos = endPos;
            }
            else
            {

                // make new Vector3 so objects dont go under floor
                nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, 1f, path[nextPosIndex].gameObject.transform.position.z);
            }
        }
        else
        {
            // move AI
            transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
        }
    }

    // Draw Path
    private void OnDrawGizmos()
    {
        Gizmos.color = color;

        // check if graph type is nav mesh
        if (graph.graphType == Graph.GraphType.NavMeshGraph)
        {
            for (int i = 0; i < path.Count; i++)
            {
                if (i == 0)
                {
                    // draw from current position to first node
                    Gizmos.DrawLine(new Vector3(startPos.x, 0.5f, startPos.z), path[i].transform.position);
                }
                else if (i == path.Count - 1)
                {
                    // draw from last node to last position
                    Gizmos.DrawLine(path[i].transform.position, new Vector3(endPos.x, 0.5f, endPos.z));

                    Gizmos.DrawLine(path[i].transform.position, path[i - 1].transform.position);
                }
                else
                {
                    // draw path
                    Gizmos.DrawLine(path[i].transform.position, path[i - 1].transform.position);
                }
            }
        }
        else // othwerwise 
        {
            for (int i = 1; i < path.Count; i++)
            {
                Gizmos.DrawLine(path[i].transform.position, path[i - 1].transform.position);
            }
        }
    }
}
