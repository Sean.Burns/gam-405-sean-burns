﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueueTest : MonoBehaviour
{
    // ~~~~~~~~~~~~~~~~~~~~~~~
    // INT TEST VARIABLES

    public int testInt1 = 5;
    public int testInt2 = 10;
    public int testInt3 = 2;
    public int testInt4 = 10;
    public int testInt5 = 4;

    public PriorityQueue<int> pqIntTest = new PriorityQueue<int>();
    public List<int> intTest;

    // ~~~~~~~~~~~~~~~~~~~~~~~
    // STRING TEST VARIABLES

    public string testStr1 = "Sean";
    public string testStr2 = "donut";
    public string testStr3 = "ate";
    public string testStr4 = "today";
    public string testStr5 = "another";

    public PriorityQueue<string> pqStrTest = new PriorityQueue<string>();
    public List<string> strTest;

    // ~~~~~~~~~~~~~~~~~~~~~~~
    // CHAR TEST VARIABLES

    public char testChr1 = 'c';
    public char testChr2 = 'a';
    public char testChr3 = 'b';
    public char testChr4 = 'd';
    public char testChr5 = 'a';

    public PriorityQueue<char> pqChrTest = new PriorityQueue<char>();
    public List<char> chrTest;

    // ~~~~~~~~~~~~~~~~~~~~~~~

    // TEST
    void Start()
    {
        // int test
        pqIntTest.Enqueue(testInt1, 5);
        pqIntTest.Enqueue(testInt2, 10);
        pqIntTest.Enqueue(testInt3, 2);
        pqIntTest.Enqueue(testInt4, 10);
        pqIntTest.Enqueue(testInt5, 4);

        intTest.Add(pqIntTest.Dequeue());
        intTest.Add(pqIntTest.Dequeue());
        intTest.Add(pqIntTest.Dequeue());
        intTest.Add(pqIntTest.Dequeue());
        intTest.Add(pqIntTest.Dequeue());

        // string test
        pqStrTest.Enqueue(testStr1, 1);
        pqStrTest.Enqueue(testStr2, 4);
        pqStrTest.Enqueue(testStr3, 2);
        pqStrTest.Enqueue(testStr4, 5);
        pqStrTest.Enqueue(testStr5, 3);

        strTest.Add(pqStrTest.Dequeue());
        strTest.Add(pqStrTest.Dequeue());
        strTest.Add(pqStrTest.Dequeue());
        strTest.Add(pqStrTest.Dequeue());
        strTest.Add(pqStrTest.Dequeue());

        // char test
        pqChrTest.Enqueue(testChr1, 3);
        pqChrTest.Enqueue(testChr2, 1);
        pqChrTest.Enqueue(testChr3, 2);
        pqChrTest.Enqueue(testChr4, 4);
        pqChrTest.Enqueue(testChr5, 1);

        chrTest.Add(pqChrTest.Dequeue());
        chrTest.Add(pqChrTest.Dequeue());
        chrTest.Add(pqChrTest.Dequeue());
        chrTest.Add(pqChrTest.Dequeue());
        chrTest.Add(pqChrTest.Dequeue());
    }
}
