using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public bool it = false;
    public int stateID = 1;

    // ai gameobject 
    public GameObject aiObj;

    // states
    public TagAIItState ItState; // state 0
    public TagAINotItState NotItState; // state 1
    public TagAIHidingState HidingState; // state 2

    // set up statemachine
    private void Start()
    {
        StartStateMachine();
    }

    // Update state we are in 
    void Update()
    {
        if (stateID == 0)
        {
            ItState.Update();
        }
        
        if (stateID == 1)
        {
            NotItState.Update();
        }
        
        if (stateID == 2)
        {
            HidingState.Update();
        }
    }

    // transition this state machine into a new state
    public void TransitionState(int newStateID)
    {
        if (stateID == 0)
        {
            // transition from it to not it
            if (newStateID == 1)
            {
                ItState.OnExitState();
                stateID = newStateID;
                NotItState.OnEnterState();
            }
        }
        else if (stateID == 1)
        {
            // transition from not it to it
            if (newStateID == 0)
            {
                NotItState.OnExitState();
                stateID = newStateID;
                ItState.OnEnterState();
            }

            // transition from not it to hiding
            if (newStateID == 2)
            {
                NotItState.OnExitState();
                stateID = newStateID;
                HidingState.OnEnterState();
            }
        }
        else if (stateID == 2)
        {
            // transition from hiding to it
            if (newStateID == 0)
            {
                HidingState.OnExitState();
                stateID = newStateID;
                ItState.OnEnterState();
            }

            // transition from hiding to not it
            if (newStateID == 1)
            {
                HidingState.OnExitState();
                stateID = newStateID;
                NotItState.OnEnterState();
            }
        }
    }

    // Set up variables based on AIType
    private void StartStateMachine()
    {
        // check if ai is it
        if (it == true) 
        { 
            // set ai to it
            stateID = 0;
            ItState.OnEnterState();
        }
        else if (it == false)
        {
            // set ai to not it
            stateID = 1;
            NotItState.OnEnterState();
        }
    }
}
