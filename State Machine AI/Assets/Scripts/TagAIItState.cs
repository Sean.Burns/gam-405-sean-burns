using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagAIItState : MonoBehaviour
{
    // base ai variables
    Color stateColor = new Color(1, 0, 0, 1);
    public StateMachine stateMachine;

    // state variables 
    private float speed = 2.5f;
    private GameObject chaseObj;

    // overrided on enter state
    public void OnEnterState()
    {
        // set it to false
        stateMachine.it = true;

        // set color to stateColor
        stateMachine.aiObj.GetComponent<Renderer>().material.color = stateColor;

        // set ai obj tag
        stateMachine.aiObj.tag = "it";
    }

    // overrided update
    public void Update()
    {
        // find ai obj to chase
        FindAI();

        // move ai
        MoveAI();

        // check for collisionn to switch states
        if (stateMachine.aiObj.transform.position == chaseObj.transform.position)
        {
            chaseObj.GetComponent<StateMachine>().TransitionState(0);
            stateMachine.TransitionState(1);
        }
    }

    // overrided on exit state
    public void OnExitState()
    {
        // rotate object 
        stateMachine.aiObj.transform.Rotate(Vector3.right, Time.deltaTime);
    }

    // find ai to chase
    private void FindAI()
    {
        // find all not it ai
        GameObject[] aiObjects = GameObject.FindGameObjectsWithTag("notIt");

        // variables
        float lowestDist = 0;
        float distance;

        // find closest ai 
        for (int i = 0; i < aiObjects.Length; i++)
        {
            // calculate distance 
            distance = Vector3.Distance(stateMachine.aiObj.transform.position, aiObjects[i].transform.position);
            
            if (lowestDist == 0)
            {
                lowestDist = distance;

                // set ai to chase
                chaseObj = aiObjects[i];
            }

            // check if lowest distance
            if (distance < lowestDist)
            {
                lowestDist = distance;

                // set ai to chase
                chaseObj = aiObjects[i];
            }
        }
    }

    // move ai
    private void MoveAI()
    {
        // move ai
        stateMachine.aiObj.transform.position = Vector3.MoveTowards(stateMachine.aiObj.transform.position, chaseObj.transform.position, speed * Time.deltaTime);
    }

    // waiting coroutine
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
    }
}
