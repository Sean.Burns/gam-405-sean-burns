using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStates
{
    public string AIType;

    public virtual void OnEnterState() { }
    public virtual void Update() { }
    public virtual void OnExitState() { }
}
