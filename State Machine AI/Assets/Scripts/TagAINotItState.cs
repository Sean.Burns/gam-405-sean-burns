using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagAINotItState : MonoBehaviour
{
    // base ai variables
    Color stateColor = new Color(1, 1, 0, 1);
    public StateMachine stateMachine;

    // ai state variables 
    public GameObject wall;
    private Vector3 destination;
    private float x;
    private float z;
    private float speed = 2;

    // overrided on enter state
    public void OnEnterState()
    {
        // set it to false
        stateMachine.it = false;

        // set color to stateColor
        stateMachine.aiObj.GetComponent<Renderer>().material.color = stateColor;

        // set ai obj tag
        stateMachine.aiObj.tag = "notIt";
    }

    // overrided update
    public void Update()
    {
        // check for wall
        WallCheck();

        // move around floor object
        MoveAI();
    }

    // overrided on exit state
    public void OnExitState()
    {
        // rotate object 
        stateMachine.aiObj.transform.Rotate(Vector3.right, Time.deltaTime);
    }

    // move ai randomly
    private void MoveAI()
    {
        if (stateMachine.aiObj.transform.position == destination || destination == null)
        {
            // get random variables to move to 
            x = Random.Range(-15, 15);
            z = Random.Range(-15, 15);

            // update current destination 
            destination = new Vector3(x, 0, z);
        }
        else
        {
            // move ai
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(stateMachine.aiObj.transform.position, destination, speed * Time.deltaTime);
        }
    }

    // check wall distance
    private void WallCheck()
    {
        float distance = Vector3.Distance(stateMachine.transform.position, wall.transform.position);

        if (distance < 2)
        {
            // if distance check correct switch state
            stateMachine.TransitionState(2);
        }
    }

    // waiting coroutine
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
    }

}
