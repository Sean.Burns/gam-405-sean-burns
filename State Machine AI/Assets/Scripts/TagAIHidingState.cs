using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagAIHidingState : MonoBehaviour
{
    // base ai variables 
    Color stateColor = new Color(0, 0, 0, 1);
    public StateMachine stateMachine;

    // state variables
    private float speed = 1;

    // overrided on enter state
    public void OnEnterState()
    {
        // set it to false
        stateMachine.it = false;

        // set color to stateColor
        stateMachine.aiObj.GetComponent<Renderer>().material.color = stateColor;

        // set ai obj tag
        stateMachine.aiObj.tag = "notIt";
    }

    // overrided update
    public void Update()
    {
        // move ai
        MoveAI();
    }

    // overrided on exit state
    public void OnExitState()
    {
        // rotate object 
        stateMachine.aiObj.transform.Rotate(Vector3.right, Time.deltaTime);
    }

    // move ai
    private void MoveAI()
    {
        // move ai slowly
        Vector3 destination = new Vector3(stateMachine.aiObj.transform.position.x + 0.2f, 0, stateMachine.aiObj.transform.position.z + 0.2f);
        Vector3 start = stateMachine.transform.position;

        if (stateMachine.transform.position == destination)
        {
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(destination, start, speed * Time.deltaTime);
        }
        else
        {
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(start, destination, speed * Time.deltaTime);
        }
    }

    // waiting coroutine
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
    }

}
