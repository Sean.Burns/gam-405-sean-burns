﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SeanBurnsAI", menuName = "AI/Sean Burns AI")]
public class SeanBurnsAI : BaseAI
{
    // DATA-----------------------------------------------------------
    private List<GameManager.Direction> goalPath = new List<GameManager.Direction>(); // list of directions to return when diamond is found
    private List<GameManager.Direction> traversed = new List<GameManager.Direction>(); // list of moves so far to quickly escape bombs without hitting wall
    private List<GameManager.Direction> clearTiles = new List<GameManager.Direction>(); // list of clear spaces to move to
    public List<GameManager.Direction> path = new List<GameManager.Direction>(); // list of directions used for movement

    // bomb timer 
    int bombTimer = 7;

    // previous move to not repeat
    GameManager.Direction previousMove;

    // long term bools (never reset)
    bool hasDiamond = false;
    bool knowGoal = false;

    // short term info bools (reset each move)
    bool nextToEnemy = false; 
    bool nextToBomb = false; 
    bool nextToDiamond = false; 
    bool nextToGoal = false;  

    // action bools (reset each move)
    bool attack = false;
    bool retreat = false;
    bool explore = false;
    bool grabDiamond = false;
    bool goToGoal = false;
    bool deliverDiamond = false;
    //----------------------------------------------------------------



    // GET ACTION-----------------------------------------------------
    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        // change bomb timer after 10 movement turns
        if (traversed.Count >= 10)
        {
            bombTimer = 3;
        }

        // set variables
        aBombTime = bombTimer;
        Reset();

        // collect information about surroundings
        CollectInfo();

        // make decision on what action to take
        MakeDecision();

        // attack
        if (attack)
        {
            return Attack();
        }

        // pass turn
        if (!attack && !retreat && !explore && !grabDiamond && !goToGoal && !deliverDiamond)
        {
            return Pass();
        }

        // create path to move based on action bools
        aMoves = CreatePath();

        // move
        return Move();
    }
    //----------------------------------------------------------------



    // RESET VARIABLES------------------------------------------------
    public void Reset()
    {
        // reset info bools
        nextToEnemy = false;
        nextToBomb = false;
        nextToDiamond = false;
        nextToGoal = false;

        // reset action bools
        attack = false;
        retreat = false;
        explore = false;
        grabDiamond = false;
        goToGoal = false;
        deliverDiamond = false;

        // reset movement path
        path.Clear();
        clearTiles.Clear();
    }
    //----------------------------------------------------------------



    // COLLECT INFO---------------------------------------------------
    public void CollectInfo()
    {
        // check if surrounding space is clear
        if (WallCheck(GameManager.Direction.Down) && OffGridCheck(GameManager.Direction.Down))
        {
            clearTiles.Add(GameManager.Direction.Down);
        }

        if (WallCheck(GameManager.Direction.Left) && OffGridCheck(GameManager.Direction.Left))
        {
            clearTiles.Add(GameManager.Direction.Left);
        }

        if (WallCheck(GameManager.Direction.Up) && OffGridCheck(GameManager.Direction.Up))
        {
            clearTiles.Add(GameManager.Direction.Up);
        }

        if (WallCheck(GameManager.Direction.Right) && OffGridCheck(GameManager.Direction.Right))
        {
            clearTiles.Add(GameManager.Direction.Right);
        }

        // check surroundings for bombs
        if ((UseSensor(GameManager.Direction.Current) & GameManager.SensorData.Bomb) != 0)
        {
            nextToBomb = true;
        }

        if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Bomb) != 0)
        {
            nextToBomb = true;
        }

        if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Bomb) != 0)
        {
            nextToBomb = true;
        }

        if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Bomb) != 0)
        {
            nextToBomb = true;
        }

        if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Bomb) != 0)
        {
            nextToBomb = true;
        }

        // Check surroundings for Enemy
        if ((UseSensor(GameManager.Direction.Current) & GameManager.SensorData.Enemy) != 0)
        {
            nextToEnemy = true;
        }

        if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Enemy) != 0)
        {
            nextToEnemy = true;
        }

        if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Enemy) != 0)
        {
            nextToEnemy = true;
        }

        if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Enemy) != 0)
        {
            nextToEnemy = true;
        }

        if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Enemy) != 0)
        {
            nextToEnemy = true;
        }

        // Check surroundings for Diamond
        if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Diamond) != 0)
        {
            nextToDiamond = true;
        }

        if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Diamond) != 0)
        {
            nextToDiamond = true;
        }

        if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Diamond) != 0)
        {
            nextToDiamond = true;
        }

        if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Diamond) != 0)
        {
            nextToDiamond = true;
        }

        // Check surroundings for Goal
        if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Goal) != 0)
        {
            nextToGoal = true;
        }

        if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Goal) != 0)
        {
            nextToGoal = true;
        }

        if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Goal) != 0)
        {
            nextToGoal = true;
        }

        if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Goal) != 0)
        {
            nextToGoal = true;
        }

        // Check if ontop of Diamond and collected it
        if ((UseSensor(GameManager.Direction.Current) & GameManager.SensorData.Diamond) != 0)
        {
            hasDiamond = true;
        }

        // Check if ontop of goal
        if ((UseSensor(GameManager.Direction.Current) & GameManager.SensorData.Goal) != 0)
        {
            knowGoal = true;
        }
    }
    //----------------------------------------------------------------



    // MAKE DECISION--------------------------------------------------
    public void MakeDecision() 
    {
        // decision hierarchy

        // AI has diamond and knows goal
        if (hasDiamond && knowGoal)
        {
            deliverDiamond = true;
            return;
        }

        // AI is next to Bomb
        if (nextToBomb)
        {
            retreat = true;
            return;
        }

        // AI is next to Enemy
        if (nextToEnemy)
        {
            attack = true;
            return;
        }

        // AI next to Diamond
        if (nextToDiamond)
        {
            grabDiamond = true;
            return;
        }

        // AI next to Goal and has not explored goal yet
        if (nextToGoal && !knowGoal)
        {
            goToGoal = true;
            return;
        }

        // otherwise explore
        explore = true;
    }
    //----------------------------------------------------------------



    // CREATE PATH----------------------------------------------------
    public List<GameManager.Direction> CreatePath()
    {
        // deliver diamond path
        if (deliverDiamond)
        {
            for(int i = 0; i < goalPath.Count; i++)
            {
                path.Add(goalPath[goalPath.Count - (i + 1)]);
            }
        }

        // grab diamond path 
        if (grabDiamond)
        {
            hasDiamond = true;

            // check which direction diamond is in and return direction
            if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Diamond) != 0)
            {
                path.Add(GameManager.Direction.Down);
            }
            else if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Diamond) != 0)
            {
                path.Add(GameManager.Direction.Up);
            }
            else if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Diamond) != 0)
            {
                path.Add(GameManager.Direction.Left);
            }
            else if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Diamond) != 0)
            {
                path.Add(GameManager.Direction.Right);
            }
        }

        // go to goal path
        if (goToGoal)
        {
            // check which direction goal is in and return direction
            if ((UseSensor(GameManager.Direction.Down) & GameManager.SensorData.Goal) != 0)
            {
                path.Add(GameManager.Direction.Down);
            }
            else if ((UseSensor(GameManager.Direction.Up) & GameManager.SensorData.Goal) != 0)
            {
                path.Add(GameManager.Direction.Up);
            }
            else if ((UseSensor(GameManager.Direction.Left) & GameManager.SensorData.Goal) != 0)
            {
                path.Add(GameManager.Direction.Left);
            }
            else if ((UseSensor(GameManager.Direction.Right) & GameManager.SensorData.Goal) != 0)
            {
                path.Add(GameManager.Direction.Right);
            }
        }

        // retreat path 
        if (retreat)
        {
            if (traversed.Count >= 10)
            {
                int rand = Random.Range(5, 10); // random range used for how many tiles to retreat

                for (int i = 0; i < rand; i++)
                {
                    path.Add(traversed[traversed.Count - (i + 1)]);
                }
            }
            else
            {
                // move two spaces in previous direction to avoid the bomb
                path.Add(previousMove);
                path.Add(previousMove);
            }
        }

        // explore path 
        if (explore)
        {
            // if enough spaces remove previous move
            if (clearTiles.Count > 1)
            {
                clearTiles.Remove(previousMove);
            }

            // get random int
            int rand = Random.Range(0, clearTiles.Count);

            // choose random tile
            path.Add(clearTiles[rand]);
        }


        // add to goal path list
        if (knowGoal)
        {
            for (int i = 0; i < path.Count; i++)
            {
                goalPath.Add(GetPastDirection(path[i]));
            }
        }

        // add to traversed list
        for (int i = 0; i < path.Count; i++)
        {
            traversed.Add(GetPastDirection(path[i]));
        }

        // add previous move for next turn
        previousMove = GetPastDirection(path[path.Count - 1]);

        return path;
    }
    //----------------------------------------------------------------



    // GET PAST DIRECTION---------------------------------------------
    public GameManager.Direction GetPastDirection(GameManager.Direction directionTraversed)
    {
        if (directionTraversed == GameManager.Direction.Down)
        {
            return GameManager.Direction.Up;
        }

        if (directionTraversed == GameManager.Direction.Up)
        {
            return GameManager.Direction.Down;
        }

        if (directionTraversed == GameManager.Direction.Left)
        {
            return GameManager.Direction.Right;
        }

        if (directionTraversed == GameManager.Direction.Right)
        {
            return GameManager.Direction.Left;
        }

        // will never return 
        return GameManager.Direction.Right;
    }
    //----------------------------------------------------------------



    // WALL CHECK-----------------------------------------------------
    public bool WallCheck(GameManager.Direction direction)
    {
        if ((UseSensor(direction) & GameManager.SensorData.Wall) == 0)
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    //----------------------------------------------------------------



    // OFF GRID CHECK-------------------------------------------------
    public bool OffGridCheck(GameManager.Direction direction)
    {
        if ((UseSensor(direction) & GameManager.SensorData.OffGrid) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //----------------------------------------------------------------



    // ACTIONS--------------------------------------------------------------------------------------------------------------------


    // ATTACK---------------------------------------------------------
    public CombatantAction Attack()
    {
        return CombatantAction.DropBomb;
    }
    //----------------------------------------------------------------



    // MOVE-----------------------------------------------------------
    public CombatantAction Move()
    {
        return CombatantAction.Move;
    }
    //----------------------------------------------------------------



    // PASS-----------------------------------------------------------
    public CombatantAction Pass()
    {
        return CombatantAction.Pass;
    }
    //----------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------------------
}
