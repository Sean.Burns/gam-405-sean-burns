using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondScript : MonoBehaviour
{
    private float rotateSpeed = 12.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // rotate diamond over time
        transform.Rotate(new Vector3(0, 10, 0), rotateSpeed * Time.deltaTime);

        /*yRotate = y * (rotateSpeed * Time.deltaTime);

        transform.rotation = Quaternion.Euler(45, yRotate, 0);*/ 

    }
}
