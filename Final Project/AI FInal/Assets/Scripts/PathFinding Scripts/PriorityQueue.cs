using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<DataType>
{
    // individual element in priority queue
    class PQElement
    {
        public DataType data;
        public int priority;
    }

    // store all data internally with a linked list
    LinkedList<PQElement> linkedList = new LinkedList<PQElement>();

    // insert data with a priority into the queue
    public void Enqueue(DataType aData, int aPriority)
    {
        // create element to be placed in queue
        PQElement tempElement = new PQElement();
        tempElement.data = aData;
        tempElement.priority = aPriority;

        if (linkedList.Count > 0)
        {
            // traversal node
            var currentNode = linkedList.First;

            // if element has a higher priority than the head of linked list
            if (linkedList.First.Value.priority > aPriority)
            {
                // adds element to front of list
                linkedList.AddFirst(tempElement);
            }
            else
            {
                // traverse list
                while (currentNode.Next != null && currentNode.Next.Value.priority < aPriority)
                {
                    currentNode = currentNode.Next;
                }

                // insert in correct position based on traversal
                linkedList.AddAfter(currentNode, tempElement);
            }
        }
        else
        {
            linkedList.AddFirst(tempElement);
        }
    }

    // return element with lowest priority
    public DataType Dequeue()
    {
        PQElement firstElement = linkedList.First.Value;
        linkedList.RemoveFirst();
        return firstElement.data;
    }

    // get size of queue
    public int Count()
    {
        return linkedList.Count;
    }
}
