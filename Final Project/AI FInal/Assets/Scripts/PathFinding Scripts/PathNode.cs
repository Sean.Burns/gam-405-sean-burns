using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    // graph that the pathnode will be added to
    [SerializeField]
    public Graph graph;

    // list of connected Nodes, gets rid of having to use another node class
    public List<PathNode> connectedNodes;

    // weight
    public float weight;

    // triangulated navmesh distance check for connection
    public float distanceCheck;

    // connect if on the same triangle
    public int triangleIdx;

    // float used for checking distance from mouse
    public float mouseDistance;

    void Awake()
    {
        if (graph != null)
        {
            graph.AddNode(this);
        }
        else if (graph == null)
        {
            GameObject graphObject = GameObject.FindGameObjectWithTag("graph");
            graph = graphObject.GetComponent<Graph>();
            graph.AddNode(this);
        }
    }
}
