using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    // movement variables
    public float playerSpeed;
    Rigidbody rb;
    Vector3 pos;
    private bool playerMoving = false;

    // diamond variables
    public GameObject diamond;
    private bool hasDiamond = false;

    // other variables
    public float hp = 100;
    public Slider healthBar;
    public float spotlightDamage;
    public float flashlightDamage;
    public bool gameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        hp = 100;
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        // movement
        pos.x = Input.GetAxisRaw("Horizontal");
        pos.z = Input.GetAxisRaw("Vertical");

        UpdateHealthBar();
    }

    // movement
    public void FixedUpdate()
    {
        if (pos.x != 0 || pos.z != 0)
        {
            rb.velocity = pos * playerSpeed * Time.fixedDeltaTime;
            playerMoving = true;
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
            playerMoving = false;
        }
    }

    // grab diamond and place diamond
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == diamond)
        {
            if (gameOver == false)
            {
                hasDiamond = true;
                collision.transform.SetParent(gameObject.transform);
                collision.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                collision.transform.position += new Vector3(0, 0.70f, 0);
            }
        }
        else if (collision.gameObject.tag == "Goal" && hasDiamond)
        {
            // win condition
            //Debug.Log("YouWin");

            // place diamond on goal
            diamond.transform.parent = null;
            diamond.transform.localScale = new Vector3(0.03f, 0.05f, 0.03f);
            diamond.transform.position = collision.transform.position;
            diamond.transform.position += new Vector3(0, 0.2f, -0.2f);

            gameOver = true;
            hasDiamond = false;
        }
        else if (collision.gameObject.tag == "enemy")
        {
            if (gameOver == false)
            {
                hp -= 33.3f;
                UpdateHealthBar();
            }
        }
    }

    // take damage from spotlight
    private void OnTriggerStay(Collider other)
    {
        if (gameOver == false)
        {
            if (other.gameObject.tag == "spotlight")
            {
                hp -= spotlightDamage;
                UpdateHealthBar();
            }

            if (other.gameObject.tag == "flashlight")
            {
                hp -= flashlightDamage;
                UpdateHealthBar();
            }
        }
    }

    //update health bar UI
    private void UpdateHealthBar()
    {
        healthBar.value = hp;
    }
}
