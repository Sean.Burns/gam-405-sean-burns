using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorAISearching : MonoBehaviour
{
    // base ai variables 
    public FloorAIStateMachine stateMachine;

    // expression
    public string expression = "?";

    // turn bools
    bool turnUp = false;
    bool turnDown = false;
    bool turnRight = false;
    bool turnLeft = false;
    bool turning = true;

    // on enter state
    public void OnEnterState()
    {
        ResetBools();

        stateMachine.expression.SetText(expression);
    }

    // update
    public void UpdateState()
    {
        // turn ai
        /*if (turning == true)
        {
            StartCoroutine(TurnUP());
            StartCoroutine(TurnRight());
            StartCoroutine(TurnDown());
            StartCoroutine(TurnLeft());
        }*/

        if (turning == true)
        {
            StartCoroutine(TurnAIWait());
        }
        
        if (turning == false)
        {
            stateMachine.TransitionState(2);
        }
        
    }

    // on exit state
    public void OnExitState()
    {
        stateMachine.expression.SetText(" ");
    }

    IEnumerator TurnAIWait()
    {
        if (turnUp == false)
        {
            transform.rotation = Quaternion.Euler(0, 90, 0);
            turnUp = true;
        }

        yield return new WaitForSeconds(2);

        if (turnRight == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            turnRight = true;
        }

        yield return new WaitForSeconds(2);

        if (turnDown == false)
        {
            transform.rotation = Quaternion.Euler(0, 270, 0);
            turnDown = true;
        }

        yield return new WaitForSeconds(2);

        if (turnLeft == false)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            turnLeft = true;
        }

        yield return new WaitForSeconds(2);

        turning = false;
    }


    IEnumerator TurnUP()
    {
        yield return new WaitForSeconds(2);
        transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    IEnumerator TurnRight()
    {
        yield return new WaitForSeconds(4);
        transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    IEnumerator TurnDown()
    {
        yield return new WaitForSeconds(6);
        transform.rotation = Quaternion.Euler(0, 270, 0);
    }

    IEnumerator TurnLeft()
    {
        yield return new WaitForSeconds(8);
        transform.rotation = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(2);
        turning = false;
    }

    IEnumerator Transition()
    {
        yield return new WaitForSeconds(1);

        turning = false;

        stateMachine.TransitionState(0);
    }

    private void ResetBools()
    {
        turnUp = false;
        turnDown = false;
        turnRight = false;
        turnLeft = false;
        turning = true;
    }

}
