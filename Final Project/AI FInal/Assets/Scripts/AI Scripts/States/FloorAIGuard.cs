using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorAIGuard : MonoBehaviour
{
    // base ai variables 
    public FloorAIStateMachine stateMachine;

    bool guarding;

    // expression
    public string expression = "!";

    // on enter state
    public void OnEnterState()
    {
        stateMachine.expression.SetText(expression);

        stateMachine.flashlight.SetActive(false);
        stateMachine.spotlight.SetActive(true);
        guarding = true;
    }

    // update
    public void UpdateState()
    {
        // guard area
        if (guarding == true)
        {
            StartCoroutine(StandGuard());
        }

        if (guarding == false)
        {
            stateMachine.TransitionState(0);
        }
        
    }

    // on exit state
    public void OnExitState()
    {
        stateMachine.expression.SetText(" ");

        stateMachine.flashlight.SetActive(true);
        stateMachine.spotlight.SetActive(false);
        guarding = true;
    }

    IEnumerator StandGuard()
    {
        yield return new WaitForSeconds(5);

        guarding = false;
    }

   
}
