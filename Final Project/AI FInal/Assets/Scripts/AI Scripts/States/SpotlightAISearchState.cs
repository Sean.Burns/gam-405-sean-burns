using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightAISearchState : MonoBehaviour
{
    // base ai variables 
    public SpotlightAIStateMachine stateMachine;

    // turning variables 
    private bool turning = true;
    public float searchTime;

    // turn variables
    public enum TurnDirections { left, right, up, down};
    public TurnDirections turnDir;

    // random turn variables
    /*public float zNegTurnCheck;
    public float zPosTurnCheck;

    public float xNegTurnCheck;
    public float xPosTurnCheck;*/

    private Quaternion original;
    private bool first = false;

    // expression
    public string expression = "!";

    // set first rotation
    private void Start()
    {
        original = transform.rotation;
    }

    // on enter state
    public void OnEnterState()
    {
        ResetVar();

        /*if (first == true)
        {
            original = transform.rotation;
            first = false;
        }*/

        stateMachine.expression.SetText(expression);
    }

    // update
    public void UpdateState()
    {
        // turn and wait
        if (turning == true)
        {
            StartCoroutine(Wait());
        }

        if (turning == false)
        {
            if (stateMachine.previousState == 0)
            {
                stateMachine.TransitionState(2);
            }
            else if (stateMachine.previousState == 2)
            {
                stateMachine.TransitionState(0);
            }
        }
    }

    // on exit state
    public void OnExitState()
    {
        ResetVar();

        stateMachine.expression.SetText(" ");
    }



    // turn ai
    public void TurnAI()
    {
        // check what move on to determine turn
        if (stateMachine.previousState == 2)
        {
            transform.rotation = original;
        }
        else 
        {
            // check which angle to rotate and rotate
            /*if (transform.position.z >= zNegTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 270, 0);
            }

            if (transform.position.z <= zPosTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 90, 0); // fix (non negative? -90)
            }

            if (transform.position.x <= xNegTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }

            if (transform.position.x >= xPosTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }*/

            if (turnDir == TurnDirections.down)
            {
                transform.rotation = Quaternion.Euler(0, 270, 0);
            }

            if (turnDir == TurnDirections.up)
            {
                transform.rotation = Quaternion.Euler(0, 90, 0); // fix (non negative? -90)
            }

            if (turnDir == TurnDirections.right)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }

            if (turnDir == TurnDirections.left)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }

    private IEnumerator Wait()
    {
        // check what move on to determine turn
        if (stateMachine.previousState == 2)
        {
            transform.rotation = original;
        }
        else
        {
            // check which angle to rotate and rotate
            /*if (transform.position.z >= zNegTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 270, 0);
            }

            if (transform.position.z <= zPosTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 90, 0); // fix (non negative? -90)
            }

            if (transform.position.x <= xNegTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }

            if (transform.position.x >= xPosTurnCheck)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }*/

            if (turnDir == TurnDirections.down)
            {
                transform.rotation = Quaternion.Euler(0, 270, 0);
            }

            if (turnDir == TurnDirections.up)
            {
                transform.rotation = Quaternion.Euler(0, 90, 0); // fix (non negative? -90)
            }

            if (turnDir == TurnDirections.right)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }

            if (turnDir == TurnDirections.left)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

        yield return new WaitForSeconds(searchTime); 

        turning = false;
    }

    private void ResetVar()
    {
        turning = true;
    }
}
