using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightAIPatrolState : MonoBehaviour
{
    // base ai variables 
    public SpotlightAIStateMachine stateMachine;

    // moving variables 
    private int nextPosIndex;
    private Vector3 nextPos;
    public bool moving = false;
    public float speed = 2;
    public int tileReserve; // reserves tiles in both directions from ai or in move dir
    public enum MoveDirection { left, right};
    public MoveDirection moveDir;

    // graph
    public Graph graph;

    // pathfinding nodes 
    PathNode endNode;
    PathNode startNode;

    // path
    public List<PathNode> path = new List<PathNode>();
    bool pathFound = false;

    // expression
    public string expression = "?";

    // on enter state
    public void OnEnterState()
    {
        if (pathFound == false)
        {
            FindPath();
        }

        stateMachine.expression.SetText(expression);
    }

    // update
    public void UpdateState()
    {

        // move ai
        MoveAI(path);

        // transition to searching state
        if (moving == false)
        {
            stateMachine.TransitionState(1);
        }
    }

    // on exit state
    public void OnExitState()
    {
        stateMachine.expression.SetText(" ");

        // reset index
        nextPosIndex = 0;
    }

    // move ai
    public void MoveAI(List<PathNode> path)
    {
        moving = true;

        if (nextPosIndex == 0)
        {
            // set first position to the first node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }
        
        if (transform.position == nextPos)// check if AI reached next node
        {
            // increase node index to get the next node in the path
            nextPosIndex++;

            // if AI is at end of path end movement
            if (nextPosIndex > path.Count)
            {
                moving = false;
                return;
            }

            // set next position to the next node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }
        else
        {
            // move AI
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
        }
    }

    // find path
    public void FindPath()
    {
        // find node to move to
        //endNode = graph.graphNodes[Random.Range(0, graph.graphNodes.Count)];
        Reserve();

        // find current node
        startNode = graph.FindClosestNode(transform.position);

        // pathfind
        path = graph.AStarSearch(startNode, endNode);
        stateMachine.ReturnState.path = path;

        pathFound = true;
    }

    // reservation system
    public void Reserve()
    {
        // selects an end node to path find within a certain distance staying away from other spotlight ai objects
        PathNode currNode = graph.FindClosestNode(transform.position);

        int coinFlip = Random.Range(0, 2);

        // random reservation
        /*if (coinFlip == 0)
        {
            endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(tileReserve * 2, 0, 0));

            // search up if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, tileReserve * 2));
            }

            // search down if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, -tileReserve * 2));
            }
        }
        else if (coinFlip == 1)
        {
            endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(-tileReserve * 2, 0, 0));

            // search up if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, tileReserve * 2));
            }

            // search down if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, -tileReserve * 2));
            }
        }*/

        // predetermined reservation
        if (moveDir == MoveDirection.left)
        {
            endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(-tileReserve * 2, 0, 0));

            // search up if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, tileReserve * 2));
            }

            // search down if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, -tileReserve * 2));
            }
        }
        else if (moveDir == MoveDirection.right)
        {
            endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(tileReserve * 2, 0, 0));

            // search up if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, tileReserve * 2));
            }

            // search down if no nodes to right
            if (endNode == currNode)
            {
                endNode = graph.FindClosestNode(currNode.transform.position + new Vector3(0, 0, -tileReserve * 2));
            }
        }
    }
}
