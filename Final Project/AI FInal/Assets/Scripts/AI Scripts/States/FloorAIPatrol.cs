using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorAIPatrol : MonoBehaviour
{
    // base ai variables 
    public FloorAIStateMachine stateMachine;

    // graph
    public Graph graph;

    // moving variables 
    private int nextPosIndex;
    private Vector3 nextPos;

    // pathfinding nodes 
    public PathNode endNode; // reservation by selecting where path will end, giving a section of map to specific ai
    PathNode startNode;

    // state variables
    bool moving = false;

    // path
    public List<PathNode> path = new List<PathNode>();

    bool pathFound = false;

    // expression
    public string expression = " ";

    // on enter state
    public void OnEnterState()
    {
        stateMachine.expression.SetText(expression);

        if (pathFound == false)
        {
            FindPath();
        }
    }

    // update
    public void UpdateState()
    {
        // move AI
        MoveAI(path);

    }

    // on exit state
    public void OnExitState()
    {
        stateMachine.expression.SetText(" ");

        nextPosIndex = 0;
    }

    // move ai
    public void MoveAI(List<PathNode> path)
    {
        moving = true;

        if (nextPosIndex == 0)
        {
            // set first position to the first node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }

        if (transform.position == nextPos)// check if AI reached next node
        {
            // increase node index to get the next node in the path
            nextPosIndex++;

            // if AI is at end of path end movement
            if (nextPosIndex > path.Count)
            {
                moving = false;

                // reverse path for next time in state
                path.Reverse();

                // change states
                stateMachine.TransitionState(1);

                return;
            }

            // set next position to the next node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }
        else
        {
            // move AI
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(transform.position, nextPos, stateMachine.speed * Time.deltaTime);
        }
    }

    // find path
    public void FindPath()
    {
        // find node to move to
        //endNode = graph.graphNodes[Random.Range(0, graph.graphNodes.Count)];

        // find current node
        startNode = graph.FindClosestNode(transform.position);

        // pathfind
        path = graph.AStarSearch(startNode, endNode);

        pathFound = true;
    }
}
