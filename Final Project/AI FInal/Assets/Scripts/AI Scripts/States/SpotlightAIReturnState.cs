using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightAIReturnState : MonoBehaviour
{
    // base ai variables 
    public SpotlightAIStateMachine stateMachine;

    // moving variables 
    private int nextPosIndex;
    private Vector3 nextPos;
    public bool moving = false;
    public float speed = 2;

    // path
    public List<PathNode> path = new List<PathNode>();

    // expression
    public string expression = "?";

    // on enter state
    public void OnEnterState()
    {
        stateMachine.expression.SetText(expression);

        nextPosIndex = path.Count - 1;
    }

    // update
    public void UpdateState()
    {
        // move ai
        MoveAI(path);

        // transition to searching state
        if (moving == false)
        {
            stateMachine.TransitionState(1);
        }
    }

    // on exit state
    public void OnExitState()
    {
        stateMachine.expression.SetText(" ");

        // reset index
        nextPosIndex = path.Count - 1;
    }

    // move ai
    public void MoveAI(List<PathNode> path)
    {
        moving = true;

        if (nextPosIndex == path.Count - 1)
        {
            // set first position to the first node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }

        if (transform.position == nextPos)// check if AI reached next node
        {
            // increase node index to get the next node in the path
            nextPosIndex--;

            // if AI is at end of path end movement
            if (nextPosIndex < 0)
            {
                moving = false;
                return;
            }

            // set next position to the next node
            nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, path[nextPosIndex].gameObject.transform.position.y + 1, path[nextPosIndex].gameObject.transform.position.z);
        }
        else
        {
            // move AI
            stateMachine.aiObj.transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
        }
    }
}
