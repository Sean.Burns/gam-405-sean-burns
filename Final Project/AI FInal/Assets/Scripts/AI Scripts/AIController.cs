using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    private int nextPosIndex;
    private Vector3 nextPos;
    private Vector3 endPos;
    float speed = 0.5f;
    public bool moving = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveAI(List<PathNode> path)
    {
        moving = true;

        // check if AI reached next node
        if (transform.position == nextPos)
        {
            // increase node index to get the next node in the path
            nextPosIndex++;

            // if AI is at end of path end movement
            if (nextPosIndex > path.Count)
            {
                moving = false;
                return;
            }

            // set next position
            if (nextPosIndex == path.Count)
            {
                // set position to end position if ai is at the last node
                nextPos = endPos;
            }
            else
            {

                // make new Vector3 so objects dont go under floor
                nextPos = new Vector3(path[nextPosIndex].gameObject.transform.position.x, 0.5f, path[nextPosIndex].gameObject.transform.position.z);
            }
        }
        else
        {
            // move AI
            transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
        }
    }
}
