using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpotlightAIStateMachine : MonoBehaviour
{
    public int stateID;

    // ai gameobject 
    public GameObject aiObj;

    // states
    public SpotlightAIPatrolState PatrolState; // state 0 
    public SpotlightAISearchState SearchingState; // state 1 
    public SpotlightAIReturnState ReturnState; // state 2 
    public int previousState;

    // expression
    public TextMeshPro expression;

    // set up statemachine
    private void Start()
    {
        StartStateMachine();
    }

    // Update state we are in 
    void Update()
    {
        if (stateID == 0)
        {
            PatrolState.UpdateState();
        }

        if (stateID == 1)
        {
            SearchingState.UpdateState();
        }

        if (stateID == 2)
        {
            ReturnState.UpdateState();
        }
    }

    // transition this state machine into a new state
    public void TransitionState(int newStateID)
    {
        if (stateID == 0)
        {
            // transition from patrol to searching
            if (newStateID == 1)
            {
                //Debug.Log("patrol to search");
                PatrolState.OnExitState();
                SearchingState.OnEnterState();
            }
        }
        else if (stateID == 1)
        {
            // transition from searching to patrol
            if (newStateID == 0)
            {
                //Debug.Log("search to patrol");
                SearchingState.OnExitState();
                PatrolState.OnEnterState();
            }

            // transition from searching to return
            if (newStateID == 2)
            {
                //Debug.Log("search to return");
                SearchingState.OnExitState();
                ReturnState.OnEnterState();
            }
        }
        else if (stateID == 2)
        {
            // transition from return to patrol
            if (newStateID == 0)
            {
                //Debug.Log("return to patrol");
                ReturnState.OnExitState();
                PatrolState.OnEnterState();
            }

            // transition from return to searching
            if (newStateID == 1)
            {
                //Debug.Log("return to search");
                ReturnState.OnExitState();
                SearchingState.OnEnterState();
            }
        }
        previousState = stateID;

        stateID = newStateID;
    }

    // Set up variables based on AIType
    private void StartStateMachine()
    {
        // set ai to patrol
        PatrolState.OnEnterState();
        stateID = 0;
    }
}
