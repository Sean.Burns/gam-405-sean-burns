using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloorAIStateMachine : MonoBehaviour
{
    public int stateID;

    // ai gameobject 
    public GameObject aiObj;

    // states
    public FloorAIPatrol PatrolState; // state 0 
    public FloorAISearching SearchingState; // state 1 
    public FloorAIGuard GuardState; // state 2 

    // expression
    public TextMeshPro expression;

    public float speed = 1;

    public GameObject flashlight;
    public GameObject spotlight;

    // set up statemachine
    private void Start()
    {
        StartStateMachine();
    }

    // Update state we are in 
    void Update()
    {

        if (stateID == 0)
        {
            PatrolState.UpdateState();
        }

        if (stateID == 1)
        {
            SearchingState.UpdateState();
        }

        if (stateID == 2)
        {
            GuardState.UpdateState();
        }
    }

    // transition this state machine into a new state
    public void TransitionState(int newStateID)
    {
        if (stateID == 0)
        {
            // transition from patrol to searching
            if (newStateID == 1)
            {
                PatrolState.OnExitState();
                SearchingState.OnEnterState();
                stateID = newStateID;
            }
        }
        else if (stateID == 1)
        {
            // transition from searching to patrol
            if (newStateID == 0)
            {
                SearchingState.OnExitState();
                PatrolState.OnEnterState();
                stateID = newStateID;
            }

            // transition from searching to guard
            if (newStateID == 2)
            {
                SearchingState.OnExitState();
                GuardState.OnEnterState();
                stateID = newStateID;
            }
        }
        else if (stateID == 2)
        {
            // transition from guard to patrol
            if (newStateID == 0)
            {
                GuardState.OnExitState();
                PatrolState.OnEnterState();
                stateID = newStateID;
            }

            // transition from guard to searching
            if (newStateID == 1)
            {
                GuardState.OnExitState();
                SearchingState.OnEnterState();
                stateID = newStateID;
            }
        }
    }

    // Set up variables based on AIType
    private void StartStateMachine()
    {
        // set ai to patrol
        PatrolState.OnEnterState();
        stateID = 0;
    }


}
