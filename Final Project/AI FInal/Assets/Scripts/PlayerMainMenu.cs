using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMainMenu : MonoBehaviour
{
    // movement variables
    public float playerSpeed;
    Rigidbody rb;
    Vector3 pos;
    private bool playerMoving = false;

    // diamond variables
    public GameObject diamond;
    private bool hasDiamond = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // movement
        pos.x = Input.GetAxisRaw("Horizontal");
        pos.z = Input.GetAxisRaw("Vertical");
    }

    // movement
    public void FixedUpdate()
    {
        if (pos.x != 0 || pos.z != 0)
        {
            rb.velocity = pos * playerSpeed * Time.fixedDeltaTime;
            playerMoving = true;
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
            playerMoving = false;
        }
    }

    // grab diamond and place diamond
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == diamond)
        {
            // collect diamond
            hasDiamond = true;
            collision.transform.SetParent(gameObject.transform);
            collision.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            collision.transform.position += new Vector3(0, 0.70f, -0.25f);
        }
        else if (collision.gameObject.tag == "StartGame" && hasDiamond)
        {
            // place diamond on button
            diamond.transform.parent = null;
            diamond.transform.localScale = new Vector3(0.03f, 0.05f, 0.03f);
            diamond.transform.position = collision.transform.position;
            diamond.transform.position += new Vector3(0, 0.2f, -0.2f);

            hasDiamond = false;

            // make selection
            StartCoroutine(StartGame());
        }
        else if (collision.gameObject.tag == "EndGame" && hasDiamond)
        {
            // place diamond on button
            diamond.transform.parent = null;
            diamond.transform.localScale = new Vector3(0.03f, 0.05f, 0.03f);
            diamond.transform.position = collision.transform.position;
            diamond.transform.position += new Vector3(0, 0.2f, -0.2f);

            hasDiamond = false;

            // make selection
            StartCoroutine(EndGame());
        }
    }

    // start game
    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // end game
    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2);

        Application.Quit();
    }
}
