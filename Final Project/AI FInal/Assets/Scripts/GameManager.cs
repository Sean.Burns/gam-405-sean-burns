using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player player;

    public int lastLevel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player.hp < 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else if (player.gameOver == true)
        {
            StartCoroutine(EndLevel());
        }
    }

    // load next level
    IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(2);

        if (SceneManager.GetActiveScene().buildIndex + 1 == lastLevel)
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
